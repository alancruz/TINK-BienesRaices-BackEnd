class CreateCoordinates < ActiveRecord::Migration[5.0]
  def change
    create_table :coordinates do |t|
      t.decimal :latitude
      t.decimal :longitude
    end
  end
end
