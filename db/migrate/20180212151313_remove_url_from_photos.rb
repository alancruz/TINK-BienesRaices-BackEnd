class RemoveUrlFromPhotos < ActiveRecord::Migration[5.0]
  def change
    remove_column :photos, :url
  end
end
