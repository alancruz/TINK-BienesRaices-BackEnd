class CreateHouses < ActiveRecord::Migration[5.0]
  def change
    create_table :properties do |t|
      t.string :area
      t.string :type_property
      t.decimal :price
      t.string :rooms
      t.string :restrooms
      t.string :yard

      t.timestamps
    end
  end
end
