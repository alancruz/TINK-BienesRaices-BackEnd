class AddReferencesToHouses < ActiveRecord::Migration[5.0]
  def change
    add_reference :photos, :property, foreign_key: true
    add_reference :coordinates, :property, foreign_key: true
    add_reference :properties, :status, foreign_key: true
  end
end
