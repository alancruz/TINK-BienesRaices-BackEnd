class CreateStatus < ActiveRecord::Migration[5.0]
  def change
    create_table :statuses do |t|
      t.string :description
      t.string :color
      t.string :opacity
    end
  end
end
