# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180212151313) do

  create_table "coordinates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.decimal "latitude",    precision: 10
    t.decimal "longitude",   precision: 10
    t.integer "property_id"
    t.index ["property_id"], name: "index_coordinates_on_property_id", using: :btree
  end

  create_table "photos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "data"
    t.integer "property_id"
    t.index ["property_id"], name: "index_photos_on_property_id", using: :btree
  end

  create_table "properties", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "area"
    t.string   "type_property"
    t.decimal  "price",         precision: 10
    t.string   "rooms"
    t.string   "restrooms"
    t.string   "yard"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "status_id"
    t.index ["status_id"], name: "index_properties_on_status_id", using: :btree
  end

  create_table "statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "description"
    t.string "color"
    t.string "opacity"
  end

  add_foreign_key "coordinates", "properties"
  add_foreign_key "photos", "properties"
  add_foreign_key "properties", "statuses"
end
