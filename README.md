# README

There is a env-sample file where you can define enviroment variables for the `database.yml` file,
Run `mv env-sample .env` on your terminal to change the name of the file and define your variables.
The .env file it's defined on the `.gitignore` file.

### Setup:
1. Install mysql(you can create a custom user or use the postgres user).
2. Install rvm
3. git clone project
3. Run gem install bundle
4. Run bundle install
5. Run Rails db:setup
