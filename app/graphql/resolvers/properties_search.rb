require 'search_object/plugin/graphql'

class Resolvers::PropertiesSearch
  include SearchObject.module(:graphql)

  scope { Property.all }

  type types[Types::PropertyType]

  PropertyFilter = GraphQL::InputObjectType.define do
    name "PropertyFilter"

    argument :OR,            -> { types[PropertyFilter] }
    argument :property_type, types.String
    argument :area,          types.String
  end

  DateFilter = GraphQL::InputObjectType.define do
    name "DateFilter"

    argument :from, type: Types::DateTimeType
    argument :to,   type: Types::DateTimeType
  end

  option :filters,   type: PropertyFilter,      with: :apply_filter
  option :first,     type: types.Int,           with: :apply_first
  option :skip,      type: types.Int,           with: :apply_skip
  option :dateRange, type: DateFilter,          with: :apply_range
  option :month,     type: Types::DateTimeType, with: :apply_month

  def apply_first(scope, value)
    scope.limit(value)
  end

  def apply_skip(scope, value)
    scope.offset(value)
  end

  def apply_filter(scope, value)
    properties = normalize_filters(value).reduce { |a, b| a.or(b) }
    scope.merge properties
  end

  def apply_range(scope, value)
    if value.size > 0 && value.size < 2
      val = value["from"] || value["to"]
      properties = Property.where(
        created_at:
          DateTime.parse(val.to_s).beginning_of_day..
          DateTime.parse(val.to_s).end_of_day)
    else
      properties = Property.where(
        created_at:
          DateTime.parse("#{value['from']}").beginning_of_day..
          DateTime.parse("#{value['to']}").end_of_day)
    end

    properties
  end

  def apply_month(_, value)
    properties = Property.where(
        created_at:
          DateTime.parse(value.to_s).beginning_of_day..
          DateTime.parse(value.end_of_month.to_s).end_of_day)
  end

  def normalize_filters(value, properties = [])
    scope = Property.all
    scope = scope.where('type_property LIKE ?', "%#{value['property_type']}%") if value['property_type']
    scope = scope.where('area LIKE ?', "%#{value['area']}%") if value['area']

    properties << scope

    value['OR'].reduce(properties) { |s, v| normalize_filters(v, s) } if value['OR'].present?

    properties
  end

end
