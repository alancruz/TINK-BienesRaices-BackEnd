Types::DateTimeType = GraphQL::ScalarType.define do
  name "DateTime"
  description "The Date scalar type enables the serialization of date data to/from iso8601"

  coerce_input ->(value, ctx) do
      begin
        value.nil? ? nil : DateTime.parse(value)
      rescue ArgumentError
        raise GraphQL::CoercionError, "cannot coerce `#{value.inspect}` to date"
      end
  end
  coerce_result ->(value, ctx) { value.nil? ? nil : value.iso8601 }
end
