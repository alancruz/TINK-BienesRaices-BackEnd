module Types
  StatusType = GraphQL::ObjectType.define do
    name "status"
    description "a status"

    field :id,          types.ID
    field :description, types.String
    field :color,       types.String
    field :opacity,     types.String
  end
end
