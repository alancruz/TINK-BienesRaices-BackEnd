module Types
  PropertyType = GraphQL::ObjectType.define do
    name "Property"
    description "a property"

    field :id,            !types.ID
    field :area,          types.String
    field :type_property, types.String
    field :price,         types.Int
    field :rooms,         types.String
    field :yard,          types.String
    field :status,        Types::StatusType
    field :createdAt,     Types::DateTimeType, property: :created_at
  end
end
