class PropertiesController < ApplicationController
  before_action :set_property, only: [:show, :update, :destroy]

  def index
    @properties = Property.all
    render json: @properties, root: false
  end

  def show
    render json: @property, root: false
  end

  def create
    params = property_params.to_h
    change_nested_attributes_names(params)
    @property = Property.new(params)
    if @property.save
      render json: @property, root: false
    else
      render json: @property.errors.full_messages, root: false, status: :bad_request
    end
  end

  def destroy
    if @property.destroy
      head :ok
    else
      render json: @property.errors.full_messages
    end
  end

  def update
    params = property_params.to_h
    change_nested_attributes_names(params)
    if @property.update(params)
      render json: @property
    else
      render json: @property.errors.full_messages
    end
  end

  private

    def property_params
      params.permit(
        :area, :type_property, :price, :rooms, :restrooms, :yard, :status_id,
        coordinates: [:id, :latitude, :longitude],
        photos:  [:id, :data, :image, :image_cache]
        )
    end

    def change_nested_attributes_names(params)
      [:photos, :coordinates].each do |attr|
        if params[attr]
          new_name = attr.to_s.concat("_attributes").to_sym
          params[new_name] = params.delete(attr)
        end
      end
    end

    def set_property
      @property = Property.find(params[:id])
    end
end
