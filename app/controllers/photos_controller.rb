require 'pry'
class PhotosController < ApplicationController
  def create
    @photo = Photo.new(photo_params)
    @photo.data = params[:photo]
    if @photo.save
       render json: @photo, root: false
    else
      render json: @photo.errors.full_messages, root: false, status: :bad_request
    end
  end

  def destroy
    @photo = Photo.find(params[:id])
    @photo.remove_data!
    if @photo.destroy
      head :ok
    else
      render json: @photo.errors.full_messages
    end
  end

  private

    def photo_params
      params.permit(:id, :data, :image, :image_cache, :property_id)
    end
end
