class Property < ApplicationRecord

  has_many :coordinates, dependent: :destroy, :inverse_of => :property
  has_many :photos, dependent: :destroy, :inverse_of => :property
  belongs_to :status

  accepts_nested_attributes_for :coordinates
  accepts_nested_attributes_for :status
  accepts_nested_attributes_for :photos
end
