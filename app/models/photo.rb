class Photo < ApplicationRecord
  mount_uploader :data, PhotoUploader
  belongs_to :property

  validates :data, presence: true
end
