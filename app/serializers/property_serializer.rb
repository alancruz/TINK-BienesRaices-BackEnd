class PropertySerializer < ActiveModel::Serializer
  attributes :id, :area, :type_property, :price, :rooms, :restrooms, :yard

  has_one :status
  has_many :coordinates
  has_many :photos
end
