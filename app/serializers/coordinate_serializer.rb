class CoordinateSerializer < ActiveModel::Serializer
  attributes :id, :latitude, :longitude
end
