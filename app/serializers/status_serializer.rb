class StatusSerializer < ActiveModel::Serializer
  attributes :id, :description, :color, :opacity
end
